package com.roofandfloor.pageTest;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class pageLoadTest{
	
	public static WebDriver driver;
	int pageLoaded = 0;
	int pageLoadFailed = 0;
	String url = "https://roofandfloor.com/hyderabad/saket-pranamam-kompally/pdp-19v9/lite";
	
	public static void launchBrowser(String url)
	{
		System.setProperty("webdriver.chrome.driver", "./src/driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(url);
	}

	public boolean pageLoadCheck()
	{
		launchBrowser(url);
		
		try
		{
			driver.findElement(By.xpath("//*[@id=\"contact-builder\"]")).isDisplayed();
			return true;
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
		finally
		{			
			driver.close();
		}

	}

	@Test
	public void test() throws InterruptedException
	{
		for (int i =0; i <100; i++)
		{
			if (pageLoadCheck())
			{
				pageLoaded++;
			}
			else
			{
				pageLoadFailed++;
			}
			System.out.println("Total number of times page loaded fine: "+pageLoaded);
			System.out.println("Total number of times page loaded failed: "+pageLoadFailed);
		}

		System.out.println("Total number of times page loaded fine: "+pageLoaded);
		System.out.println("Total number of times page loaded failed: "+pageLoadFailed);
	}
}